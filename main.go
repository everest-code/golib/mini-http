package main

import (
	"net/http"
	"os"

	"github.com/labstack/echo/v4"
	"gitlab.com/everest-code/golib/log"
)

var logger *log.Logger

func loggerMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		method, url := ctx.Request().Method, ctx.Request().URL
		ip, userAgent := ctx.RealIP(), ctx.Request().Header.Get("User-Agent")

		logger.Info("Request=\"%s %s\" IP=\"%s\" UserAgent=\"%s\"",
			method, url, ip, userAgent)

		return next(ctx)

	}
}

func main() {
	portExpose := ":8080"
	if len(os.Args) == 2 {
		portExpose = os.Args[1]
	}

	logger, _ = log.NewConsole(log.LevelDebg, false)
	logger.ImportantInfo("Listening on port: %s", portExpose)

	server := echo.New()
	server.Use(loggerMiddleware)
	server.Static("/", "./")

	listener := http.Server{
		Addr:    portExpose,
		Handler: server,
	}

	if err := listener.ListenAndServe(); err != http.ErrServerClosed {
		logger.Panic(err.Error())
	}
}
