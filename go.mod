module gitlab.com/everest-code/golib/mini-http

go 1.16

require (
	github.com/labstack/echo/v4 v4.7.0
	gitlab.com/everest-code/golib/log v1.0.1
)
